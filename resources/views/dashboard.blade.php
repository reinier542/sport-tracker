@extends('layouts.app')

@section('content')
<div class="row align-items-center mb-5">
    <div class="col-8 pr-0"><h1>Sport tracker</h1></div>
    <div class="col-4 pl-0"><a href="{{ route('getExercises')}}" class="btn btn-success btn-block"><i class="fas fa-dumbbell"></i><br>toevoegen</a></div>
</div>


<h2>Mijn workouts</h2>
@if(count($workouts) >= 1)
<table class="table table-hover table-sm">
    <tbody>
        @foreach($workouts as $workout)
        <tr class="clickable-row" data-href="{{ route('getWorkouts', with($workout->exercise->id)) }}">
            <td>
                {{ $workout->exercise->name }}
            </td>
            <td>
                Laatste: {{ date('d-m-Y', strtotime($workout->date)) }}
            </td>
            <td class="text-right">
                <button type="button" class="btn btn-primary"><i class="fas fa-angle-right"></i></button> 
            </td>
        </tr>
        @endforeach
    </tbody>
    @else
        <div class="text-secondary">Je hebt nog geen workouts. Voeg je eerste toe!</div>
    @endif
</table>

@endsection
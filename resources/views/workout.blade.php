@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="/" class="btn btn-primary mb-3"><i class="fas fa-angle-left"></i> Terug naar dashboard</a>
        <div class="row align-items-center mb-5">
            <div class="col-8 pr-0"><h1>{{ $exercise->name}}</h1></div>
            <div class="col-4 pl-0"><a href="{{ route('getWorkoutCreate', with($exercise->id)) }}" class="btn btn-success btn-block"><i class="fas fa-dumbbell"></i><br>toevoegen</a>
        </div>
</div>


    </div>
</div>
@foreach($workouts as $workout)


<div class="row">
    <div class="col-8"><h2>{{ date('d-m-Y', strtotime($workout->date)) }}</h2></div>
    <div class="col-4 pl-0">
        <button-delete :workout="{{ json_encode($workout->id) }}" :exercise="{{ json_encode($workout->exercise->id) }}"></button-delete>
    </div>
    <div class="col-12">
        <table class="table table-sm mb-4">
            <thead>
                <tr>
                    <th scope="col">Set</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($workout->sets as $set)
                <tr>
                    <th scope="row">
                        {{ $loop->iteration }}
                    </th>
                    <td>
                        {{ $set->reps }} {{ $set->reps == '1' ? 'Rep' : 'Reps' }}
                    </td>
                    <td>
                        {{ $set->weight}} kg
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endforeach

@endsection
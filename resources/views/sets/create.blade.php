@extends('layouts.app')

@section('content')

<h1>{{ $exercise->name }} toevoegen</h1>
<add-set :exercise="{{ json_encode($exercise) }}" :workout="{{ isset($workout) ? json_encode($workout) : json_encode(false) }}"></add-set>
@endsection
@extends('layouts.app')

@section('content')
<h1>Oefening toevoegen</h1>

<form method="POST" action="{{ route('postAddExercise') }}">
    @csrf
    <div class="form-group row">
        <label for="exercise" class="col-sm-4 col-form-label text-md-right">Oefening</label>

        <div class="col-md-6">
            <input id="exercise" type="text" class="form-control" name="exercise" required autofocus>
        </div>
    </div>

    <div class="form-group row">
        <label for="exerciseCategory" class="col-md-4 col-form-label text-md-right">Categorie</label>

        <div class="col-md-6">
            <select class="form-control" id="exerciseCategory" name="exerciseCategory">
                @foreach($exerciseCategories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Toevoegen
            </button>
        </div>
    </div>
</form>
@endsection
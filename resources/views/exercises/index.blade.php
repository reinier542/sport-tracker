@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="/" class="btn btn-primary mb-3"><i class="fas fa-angle-left"></i> Terug naar dashboard</a> 
        <h1>Workout toevoegen</h1>
    </div>
</div>
@foreach($exerciseCategories as $category)
<div class="list-group">
<h2 class="mt-4">{{ $category->name }}</h2>
@foreach($category->exercises as $exercise)
  <a href="{{ route('getWorkoutCreate', with($exercise->id)) }}" class="list-group-item list-group-item-action">{{ $exercise->name }}</a>
  @endforeach
</div>
@endforeach
@endsection
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}

$( document ).ready(function() {
    $('#datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "nl",
        todayBtn: 'linked',
        todayHighlight: true,
        autoclose: true,
    });
    
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./bootstrap-datepicker.min');
require('./bootstrap-datepicker.nl.min');


require('./general');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// import moment from 'moment';

// Vue.prototype.moment = moment;

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('add-set', require('./components/AddSet.vue'));

Vue.component('datepicker', {
    template: '<input type="text" v-datepicker id="datepicker" class="form-control" :value="value" @input="update($event.target.value)">',
    directives: {
        datepicker: {
            inserted(el, binding, vNode) {
                $(el).datepicker({
                    format: "dd-mm-yyyy",
                    language: "nl",
                    todayBtn: 'linked',
                    todayHighlight: true,
                    autoclose: true,
                }).on('changeDate', function (e) {
                    vNode.context.$emit('input', e.format(0))
                })
            }
        }
    },
    props: ['value'],
    data() {
        return {
            data: this.value,
        }
    },
    methods: {
        update(v) {
            this.$emit('input', v)
        }
    }
})

import swal from "sweetalert";
import Axios from "axios";

Vue.component('button-delete', {
    template: '<button class="btn btn-danger button-delete float-right" @click="deleteWorkout()"><i class="fas fa-trash-alt"></i></button>',

    props: ['workout', 'exercise'],
    data() {
        return {
        }
    },
    methods: {
        deleteWorkout() {
            swal({
                title: "Weet je het zeker?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                  console.log('ja');
                  Axios.post("/workout/delete/" + this.workout, {
                    sets: this.sets,
                    date: this.date,
                    exercise: this.exercise.id
                  })
                if (willDelete) {
                  swal("Workout is verwijderd!", {
                    icon: "success",
                  }).then(result => {
                    window.location.replace('/workouts/'+this.exercise);
                  });
                }
              });
          },
    }
})

const app = new Vue({
    el: '#app'
});

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten minimaal 6 karakters lang zijn en gelijk zijn aan elkaar.',
    'reset' => 'Je wachtwoord is gereset!',
    'sent' => 'We hebben je een wachtwoord resetlink gemaild!',
    'token' => 'Deze reset-token is niet geldig.',
    'user' => "We kunnen geen gebruiker vinden met dat E-mailadres.",

];

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    protected $with = ['exercise'];

    public $incrementing = true;

    public function sets() {
        return $this->hasMany('App\Set');
    }

    public function exercise() {
        return $this->belongsto('App\Exercise');
    }
}

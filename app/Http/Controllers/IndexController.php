<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workout;

use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboard()
    {
        $workouts = Workout::where('user_id', '=', Auth::id())->with('sets')->orderBy('date', 'desc')->get()->unique('exercise_id');

        return view('dashboard', compact('workouts'));
    }
}

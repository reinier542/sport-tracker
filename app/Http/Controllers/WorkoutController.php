<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workout;
use App\Exercise;
use Carbon\Carbon;
use App\Set;

use Illuminate\Support\Facades\Auth;

class WorkoutController extends Controller
{
    public function getWorkouts($exerciseId)
    {
        $exercise = Exercise::where('id', $exerciseId)->first();

        $workouts = Workout::where('user_id', Auth::id())->where('exercise_id', $exerciseId)->with('sets')->orderBy('date', 'desc')->get();

        return view('workout', with(['workouts' => $workouts, 'exercise' => $exercise]));
    }

    public function getCreate($exerciseId)
    {
        $exercise = Exercise::where('id', $exerciseId)->first();

        return view('sets.create', compact('exercise'));
    }

    public function postCreate(Request $request)
    {
        $workout = new Workout();
        $workout->exercise_id = $request->get('exercise');
        $workout->user_id = Auth::id();
        $workout->date = Carbon::parse($request->get('date'));
        $workout->save();

        foreach($request->sets as $key => $workoutSet) {
            
            $set = new Set();
            $set->workout_id = $workout->id;
            $set->reps = $workoutSet['reps'];
            $set->weight = $workoutSet['weight'];
            $set->pos = $key;
            $set->save();
        }

        return http_response_code(200);
    }

    public function deleteWorkout($id) {
        Set::where('workout_id', $id)->delete();

        $workout = Workout::where('id', $id)->with('sets')->first();

        $workout->delete();

        return http_response_code(200);
    }
}

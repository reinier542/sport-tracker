<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\ExerciseCategory;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function getIndex()
    {
        $exerciseCategories = ExerciseCategory::with('exercises')->get();
        
        return view('exercises.index', compact('exerciseCategories'));
    }

    public function getAdd()
    {
        $exerciseCategories =  ExerciseCategory::all();
        return view('exercises.createEdit', compact('exerciseCategories'));
    }

    public function postAdd(Request $request)
    {
        $exercise = new Exercise();
        $exercise->name = $request->input('exercise');
        $exercise->exercise_category_id = $request->input('exerciseCategory');
        $exercise->save();

        return redirect(route('getExercisesIndex'));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    public function workouts() {
        return $this->hasMany('Workout');
    }

    public function ExerciseCategory()
    {
        return $this->belongsTo('App\ExerciseCategory');
    }
}

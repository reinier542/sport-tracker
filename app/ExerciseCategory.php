<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseCategory extends Model
{
    public function exercises() {
        return $this->hasMany('App\Exercise');
    }
}

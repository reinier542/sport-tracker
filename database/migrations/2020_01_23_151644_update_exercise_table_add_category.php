<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateExerciseTableAddCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exercises', function(Blueprint $table) {
            $table->unsignedInteger('exercise_category_id')->after('name');
            $table->foreign('exercise_category_id')->references('id')->on('exercise_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exercises', function (Blueprint $table) {
            $table->dropForeign(['exercise_category_id']);
            $table->dropColumn('exercise_category_id');
        });
    
    }
}

<?php

use Illuminate\Database\Seeder;

class ExercisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exercises = [
            [
                'Benen',
                ['Squats', 'Leg press', 'Leg extensions', 'Leg curls']
            ],
            [
                'Rug',
                ['Deadlifts', 'Rows', 'Lat pulldown']
            ],
            [
                'Borst',
                ['Bench press', 'Incline bench press', 'Cable press']
            ]
        ];

        $i = 0;
        foreach($exercises as $exerciseCategory) {
            DB::table('exercise_categories')->insert([
                'name' => $exerciseCategory[0],
            ]);

            foreach($exerciseCategory[1] as $exercise) {
                DB::table('exercises')->insert([
                    'name' => $exercise,
                    'exercise_category_id' => ($i + 1)
                ]);
            }

            $i++;
        }
    }
}

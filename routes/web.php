<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'IndexController@getDashboard');

    Route::get('/exercises/add', 'ExerciseController@getAdd')->name('getAddExercise');
    Route::post('/exercises/add', 'ExerciseController@postAdd')->name('postAddExercise');

    Route::get('/workouts/{exerciseId}', 'WorkoutController@getWorkouts')->name('getWorkouts');
    Route::get('/workout/add', 'ExerciseController@getIndex')->name('getExercises');
    Route::get('/workout/add/{exerciseId}', 'WorkoutController@getCreate')->name('getWorkoutCreate');
    Route::post('/workout/add/{exerciseId}', 'WorkoutController@postCreate')->name('postSetCreate');
    Route::post('/workout/delete/{workoutId}', 'WorkoutController@deleteWorkout')->name('deleteWorkout');
});